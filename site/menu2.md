@def title = "Class Notes"

# Class Notes

![Alt Text](/assets/notes3.jpg)

## Final Exam Review 

* Monday, Nov 29th
    - [Part 2](https://tinyurl.com/35eyeac2)
    - Missed the lecture, No worries [click here](https://texastech.zoom.us/rec/share/xbH8eZLhbNCEd2_BVNT44-UXHGf6mQJbFvhc9micOQlZWruHsLu2tiDd2VVSksZS.DFgdfnV5ah0C2zuV?startTime=1638230710000)

* Monday, Nov 22nd
    - [Part 1](https://tinyurl.com/2p9b85u7)
    - Missed the lecture, No worries [click here](https://youtu.be/vKIJ7tRAxx8)

## Week 15

* Tuesday, Nov. 30th
    - [Chapter  8.7 - please watch the video to see the note]()
    - [Chapter  5.5 - please watch the video to see the note]()
    - Missed the lecture, No worries [click here](https://tinyurl.com/yckrm82p)

## Week 14

* Tuesday, Nov. 23rd
    - [Chapter 5.4](https://tinyurl.com/mtvstx8a)
    - [Chapter 5.2](https://tinyurl.com/4jcmzzyw)
    - Missed the lecture, No worries [click here](https://texastech.zoom.us/rec/share/DWn4_wDxCgYt_kJ-dcDNFrPCmlJzjzBICp0U_1vnFmMUoiIEwjlemz3D_5E2A_aA.jYs9Cd5APUDXxcd2?startTime=1637697680000)


## Week 13

* Thursday, Nov. 18th
    - [Chapter 5.1](https://tinyurl.com/sxa45rsn)
    - [Chapter 4.5 (part 2)](https://tinyurl.com/3wbaw366)
    - Missed the lecture, No worries [click here - from MWF class](https://youtu.be/OVsj-dxekyI)
    - Missed the lecture, No worries [click here - from MWF class](https://youtu.be/p5yWQWD9I5g)

  
* Tuesday, Nov. 16th
    - [Chapter 4.5 (part 1)](https://tinyurl.com/95mjb92f)
    - [Chapter 4.4](https://tinyurl.com/4zzbh4rt)
    - [Chapter 4.3(part 2)](https://tinyurl.com/2as8e5dt)
    - Missed the lecture, No worries [click here](https://youtu.be/LnQK8qnBOWU)


## Week 12

* Thursday, Nov. 11th
    - [Chapter  4.2 and 4.3 (part 1)](https://tinyurl.com/ytaduhdf)
    - [Chapter  4.1](https://tinyurl.com/2cdjc837)
    - Missed the lecture, No worries [click here](https://youtu.be/UDuU3ZZH0Ic)

* Wednesday, Nov. 10th
    - [Exam 3 review](https://tinyurl.com/2tu2rryd) 
    - Missed the review, No worries [click here](https://texastech.zoom.us/rec/share/RvGMaIchOOubkkmA1umo6NjG0Bmsf38iHgUsTTkh27z8jXyYMOg4LVGQZRu8LXaT.cE3ZUcft0l2pbKMm?startTime=1636588941000) 

    
* Tuesday, Nov. 9th
    - [Chapter 3.6](https://tinyurl.com/5df2rcmv)
    - [Chapter 3.5 (part 2)](https://tinyurl.com/ej335m2)
    - Missed the lecture, No worries [click here](https://youtu.be/zG66EQpXhq4)


## Week 11

* Thursday, Nov. 4th
    - [Chapter 3.5 (part 1)](https://tinyurl.com/5h73458k)
    - [Chapter 3.4 (part 2)](https://tinyurl.com/yjfrwm4t)
    - Missed the lecture, No worries [click here](https://youtu.be/cbLOtIbGres)

* Tuesday, Nov. 2nd
    - [Chapter 3.4 (part 1)](https://tinyurl.com/vmpe6777)
    - [Chapter 3.3 (part 2)](https://tinyurl.com/ej335m2)
    - Missed the lecture, No worries [click here](https://youtu.be/OkksFq_6Q_A)

## Week 10

* Thursday, Oct. 28st
    - [Chapter 3.3 (part 1)](https://tinyurl.com/ej335m2)
    - [Chapter 3.2 (part 2)](https://tinyurl.com/64bptknb)
    - Missed the lecture, No worries [click here](https://youtu.be/Zk0LmQZIaUo)

* Tuesday, Oct. 26th
    - [Chapter 3.2 (part 1)](https://tinyurl.com/ypd5jxtb)
    - [Chapter 3.1 (part 2)](https://tinyurl.com/2ppzu3ah)
    - Missed the lecture, No worries [click here](https://youtu.be/8M7NtKI9BiM)


## Week 9

* Thursday, Oct. 21st
    
    - Missed the review, No worries [click here](https://texastech.zoom.us/rec/share/4ZQO1MEV503G7LghE0NCz9Qn2tUwa95RAOgslJHTkbjsBIZJ5TlbQGnxHvr13q9N.hcvlQeof0ox982WP?startTime=1634842856000)

* Tuesday, Oct. 19th
    - [Chapter 3.1 (part 1)](https://tinyurl.com/2ppzu3ah)
    - [Chapter 2.7](https://tinyurl.com/zphydaup)
    - Missed the lecture, No worries [click here](https://youtu.be/fB9SlNpu82c)

## Week 8

* Thursday, Oct. 14th
    - [[Chapter 2.6](https://tinyurl.com/zphydaup)
    - Missed the lecture, No worries [click here](https://youtu.be/JQ5URE-BvLc)

* Tuesday, Oct. 12th
    - [Chapter 2.5](https://tinyurl.com/bh2pxanz)
    - Missed the lecture, No worries [click here](https://youtu.be/m8uehwYT_vc)


## Week 7 

* Thursday, Oct. 7th
    - [Chapter 2.3 and 2.4 (part 2)](https://tinyurl.com/mmy9xf42)
    - Missed the lecture, No worries [click here](https://youtu.be/1ZKpJx-R_0w)

* Tuesday, Oct. 5th
    - [Chapter 2.3 and 2.4 (part 1)](https://tinyurl.com/2a83tyh3)
    - Missed the lecture, No worries [click here](https://youtu.be/QacBapiZN7M)


## Week 6 

* Thursday, Sep. 30th
    - [Chapter 2.1 and 2.2 (part 1)](https://tinyurl.com/53d9e95m)
    - Missed the lecture, No worries [click here](https://youtu.be/8YFPqjmmNd0)

* Tuesday, Sep. 28th
    - [Chapter 1.7](https://tinyurl.com/ph96dbfn)
    - Missed the lecture, No worries [click here](https://youtu.be/pLmQqKpQCH8)


## Week 5 

* Thursday, Sep. 23rd
    - [Exam 1 is available on Blackboard](https://ttu.blackboard.com/)


* Tuesday, Sep. 21st
    - [Chapter 1.6 (part2)](https://tinyurl.com/22nesw9n)
    - [Few review problems](https://tinyurl.com/mf274va2)
    - Missed the lecture, No worries [click here](https://youtu.be/IUHL2cDJf00)


## Week 4 


* Thursday, Sep. 16th
    - [Chapter 1.6 (part1)](https://tinyurl.com/dz33xkjf)
    - Missed the lecture, No worries [click here](https://youtu.be/IhEFVDyKDb4)

* Tuesday, Sep. 14th
    - [Chapter 1.5 (part 2)](https://tinyurl.com/yrvjbf5y)
    - Missed the lecture, No worries [click here](https://youtu.be/9C_Fv0Cy-hs)

## Week 3 

* Thursday, Sep. 9th
    - [Chapter 1.5 (part 1)](https://tinyurl.com/hjbhaye7)
    - [Chapter 1.4 (part 2)](https://tinyurl.com/re5ed5x2)
    - Missed the lecture, No worries [click here](https://youtu.be/hLVzZk4JA1w)

* Tuesday, Sep. 7th
    - [Chapter 1.4 (part 1)](https://tinyurl.com/re5ed5x2)
    - [Chapter 1.3 (part 2)](https://tinyurl.com/3c9kytwd)
    - Missed the lecture, No worries [click here](https://youtu.be/fBzUuEMCSyo)

## Week 2 

* Thursday, Sep. 2nd
    - [Ch 1.3 (part 1)](https://tinyurl.com/3c9kytwd)
    - [Ch 1.2 (part 2)](https://tinyurl.com/2mpmefpc)


* Tuesday, Aug. 31st
    - [Ch 1.2 (part 1)](https://tinyurl.com/wtmmk5fu)
    - [Ch 1.1](https://tinyurl.com/2xv2k244)
    - [Review 2](https://tinyurl.com/6zmevc8p)
    - Missed the lecture, No worries [click here](https://youtu.be/CRT_C9CVXF4)

## Week 1 

* Thursday, Aug. 26th
    - [Review 1](https://tinyurl.com/6zmevc8p)

* Tuesday, Aug. 24th
    - [Welcome slides!](https://tinyurl.com/6kjpcnuv)



