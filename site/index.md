@def title = "Welcome"
@def tags = ["syntax", "code"]

# Welcome to the World of College Algebra! 

 
<!-- \tableofcontents  you can use \toc as well -->

**_MATH 1320: Most important math class you will ever take!_**

![Alt Text](/assets/toronto.gif)

---

* [Exam 2 (10/21/2021) instructions lives here!](https://tinyurl.com/4ktdjv7r)
* [Exam 1 (09/23/2021) instructions lives here!](https://tinyurl.com/2vuyj225)

---

[Anonymous Lecture Feedback](https://forms.gle/PwAcJftswqb4TCU47)

---

* Head over to the [MyLab](https://mlm.pearson.com/northamerica/)
* Head over to the [Blackboard](https://ttu.blackboard.com/)

---

## Things to be known:


----

Skeleton notes [lives here!](https://tinyurl.com/3uje6up9)

Course syllabus [lives here!](https://tinyurl.com/kd4tnpxc)


* **Lecture:** TuTr: 2:00-3:20pm, MCOM 053 
    - Live class session via ZOOM: [Click here!!!](https://zoom.us/j/8065430626)  
* **Instructor:** Bhagya Athukorallage, PhD
* **Office Hours:**  MWF: 11:00am - 11:50am & TuTr: 3:30pm - 4:30pm or by appointments
    - [Click here to enter my ZOOM office](https://zoom.us/j/8065430626)
* **Office:** MATH 253
* **E-mail:** bhagya.athukorala@ttu.edu







