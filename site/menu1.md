@def title = "Announcements"

# Announcements

![Alt Text](/assets/news.jpg)


## Week 8 

* Quiz 5 is posted in MyLab.
* Chapter 2.5 homework is posted in MyLab.


## Week 7 

* Quiz 4 is posted in MyLab.
* Chapter 2.3-2.4 homework is posted in MyLab.

## Week 6 

* Chapter 2.1-2.2 homework is posted in MyLab.


## Week 5 

* Reminder: Exam 1 - 09/23/2021
* Chapter 1.7 homework is posted in MyLab.


## Week 4 

* Quiz 3 is posted in MyLab.
* Chapter 1.6 homework is posted in MyLab.


## Week 3 

* Quiz 2 is posted in MyLab:  Due on Sunday, Sep. 12th. 
* Chapter 1.5 homework is posted in MyLab: Due on Thursday, Sep.16th. 
* Chapter 1.4 homework is posted in MyLab: Due on Tuesday, Sep. 14th.


## Week 2 

* Quiz 1 is posted in MyLab:  Due on Monday, Sep. 5th. 
* Chapter 1.3 homework is posted in MyLab: Due on Monday, Sep. 10th. 
* Chapter 1.2 homework is posted in MyLab: Due on Monday, Sep. 8th. 
* Chapter 1.1 homework is posted in MyLab: Due on Monday, Sep. 6th. 


## Week 1 

* Install [Proctorio Chrome Extension](https://getproctorio.com/) 
     @@colbox-blue
     [Proctorio Quick Start Guide](https://drive.google.com/file/d/1L3AUF83Pq3Wz2aVIw6mVoTYKpWG1uEc7/view?usp=sharing)
     @@

* HW 2 is posted in MyLab: Due on Tuesday, Sep. 3rd. 
* HW 1 is posted in MyLab: Due on Tuesday, August. 31st. 




